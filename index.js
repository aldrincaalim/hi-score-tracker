const { json } = require("express");
const express = require("express");

const app = express();

const scores = [];

app.get("/scores", (req, res) => {
    res.status(500);
    res.send("Not  implemented yet");
});

app.post("/scores", (req, res) => {
    // {initials: "AAA", points: 1000}
    const score = req.body;
    scores.push(score);
    res.send("Your new score has been successfully added");
});

app.listen(3000, () => console.log(`APP listening on port 3000`));